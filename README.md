<p align="center"><img src="public/images/meedu.png"/></p>
<p align="center">MeEdu - 知识付费解决方案。</p>
<p align="center">
<a href="https://github.styleci.io/repos/127536154"><img src="https://github.styleci.io/repos/127536154/shield?branch=master" alt="StyleCI"></a>
<a href="https://travis-ci.org/Qsnh/meedu"><img src="https://travis-ci.org/Qsnh/meedu.svg?branch=master" alt="Build Status"></a>
<a href="https://codecov.io/gh/Qsnh/meedu">
  <img src="https://codecov.io/gh/Qsnh/meedu/branch/master/graph/badge.svg" />
</a>
</p>

## 使用限制

> 本限制自 MeEdu v4.4 开始生效。

+ 本项目不限制个人，公司等主体自用（举个例子：您通过 MeEdu 搭建线上教育平台出售课程这样的商业行为是允许的）。自用请到该网址 [MeEdu自用授权登记](https://jinshuju.net/f/xMyado) 进行授权登记，未登记用户视为未获得自用授权。
+ 在未获得 **MeEdu官方(杭州白书科技有限公司)** 销售授权的情况下，禁止通过「**直接销售**」或者「**对本项目二开之后销售**」获取任何形式经济利益。

## 常用链接

- [MeEdu 官网](https://meedu.vip)
- [MeEdu 付费解决方案](https://meedu.vip/price.html)
- [MeEdu 付费版本演示站](https://meedu.vip/cases.html)
- [MeEdu 功能概览](https://www.yuque.com/meedu/fvvkbf/gpx5ed)
- [MeEdu 使用手册](https://www.yuque.com/meedu/fvvkbf)
- [MeEdu API文档](https://meedu-v2-xiaoteng.doc.coding.io/)
- [MeEdu 后台前端项目](https://github.com/Meedu/backend-v2)

## MeEdu 安装

- [x] [腾讯云云开发安装部署](https://app.cloud.tencent.com/?app=MeEdu)
- [x] [宝塔一键安装（技术能力较弱）](https://www.yuque.com/meedu/fvvkbf/qvb006)
- [x] [手动安装教程（技术能力较强）](https://www.yuque.com/meedu/fvvkbf/hhl2wk)
- [x] [MeEdu 宝塔手动安装最新版(手把手教学)](https://www.yuque.com/meedu/fvvkbf/gkape0)
